package kmermapper2;

import org.apache.spark.sql.api.java.UDF1;


public class AddNum implements UDF1 <long, long>{

		private static final long serialVersionUID = 1L;
		@Override
		public Long call(Long num) throws Exception {
		return (num + 5);
		}
		
	}
}
