package kmermapper2;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Sean Saki
 * @date 7/27/2021
 * Outline: input is a DNA sequence and settings
 * output is either a Long array of kmers or minimzers depending on the input from the User
 * organize input output 
 * data to TSV file
 */

public class kmermapper {
	
	public static void help() {// help function
		System.out.println("This program takes in three different arguements and outputs a Long array with with either kmer or minimizers depending on your input.");
		System.out.println("A standard run of the program will look like this \"kmermapper.java 30 15 ATCGATCG\"");
		System.out.println("30 represents the size of the kmer 15 represents the window size for the minimzers and the ATCG is the sequence of DNA.");
		System.out.println("If you do not want to run the program in minimizer version simply input 0 for the size of the window.");
		return;
	}

	public static long hash(long key) {
		key = (~key) + (key << 21); // key = (key << 21) - key - 1;
		key = key ^ (key >>> 24);
		key = (key + (key << 3)) + (key << 8); // key * 265
		key = key ^ (key >>> 14);
		key = (key + (key << 2)) + (key << 4); // key * 21
		key = key ^ (key >>> 28);
		key = key + (key << 31);
		return key;
	}

	public static long StringEncode(char s1, char s2) {
		if (s1 == '1') {
			if(s2=='1') {
				return 3; 
			}else if (s2 =='0') {
				return 2; 
			}
		}else if(s1 == '0') {
			if(s2=='1') {
				return 1;
			}else if (s2 =='0') {
				return 0;
			}
		}
		return -1;
	}
	
	public static long bitEncode(char ch) { // returns corresponding long value of each character
		if (ch == 'a' || ch == 'A') {
			return 0;
		} else if (ch == 'c' || ch == 'C') {
			return 1;
		} else if (ch == 'g' || ch == 'G') {
			return 2;
		} else if (ch == 't' || ch == 'T') {
			return 3;
		} else {
			return -1;
		}
	}

	public static long bitEncodeRev(char ch) { 
		if (ch == 'a' || ch == 'A') {// reads A returns C
			return 1;
		} else if (ch == 'c' || ch == 'C') {// reads C returns A
			return 0;
		} else if (ch == 'g' || ch == 'G') {// reads G returns T
			return 3;
		} else if (ch == 't' || ch == 'T') {// reads T returns G
			return 2;
		} else {
			return -1;
		}
	}
	
	public static byte[] longToBytes(long x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
	    buffer.putLong(x);
	    return buffer.array();
	}
	
	public static Long[] ReadSeq(String str, int k, int w){
		long kmer = 0;// Stores the current kmer being built and analyzed
		long kmerRev = 0; // stores the current reverse of the kmer being built
		long minimizer = 0; 
		long topMinCode = Long.MIN_VALUE;
		long PreviousCode = Long.MIN_VALUE;
		int len = 0;// Length bases since last N encountered
		final int shift = 2 * k;
		final long mask = (shift > 63 ? -1L : ~((-1L) << shift));
		char[] ch = str.toCharArray();
		ArrayList<Long> y = new ArrayList<Long>(Arrays.asList());
		if(w <= 0) { // kmer no minimizer
			for (int i = 0; i < ch.length; i++) {
				long x = bitEncode(ch[i]);
				long x2 = bitEncodeRev(ch[i]);
				kmer = ((kmer << 2) | x) & mask;// Shift the new 2 bits into the kmer
				kmerRev = ((kmerRev << 2) | x2) & mask;// Shift the new 2 bits into the kmer
				if (x < 0) {// If an N is encountered, reset the length
					len = 0;
				} else {
					len++;
				}

				if (len >= k) {// If the kmer is valid and full length, hash it
					final long code = hash(kmer);// Some arbitrary hash function
					final long code2 = hash(kmerRev);
					if(code2 <= code) {// select the kmer then add to list 
						y.add(code);

					}else {
						y.add(code2);
					}
				}
		}
		
	}else {//minimizer version
		final int wshift = 2 * w;
		final long Wmask = (wshift > 63 ? -1L : ~((-1L) << wshift));
		for (int i = 0; i < ch.length; i++) {
			long x = bitEncode(ch[i]);
			long x2 = bitEncodeRev(ch[i]);
			kmer = ((kmer << 2) | x) & mask;// Shift the new 2 bits into the kmer
			kmerRev = ((kmerRev << 2) | x2) & mask;// Shift the new 2 bits into the kmer
			if (x < 0) {// If an N is encountered, reset the length
				len = 0;
			} else {
				len++;
			}

			if (len >= k) {// If the kmer is valid and full length, break it into its minimizers after hashing it
				final long code = hash(kmer);// Some arbitrary hash function
				final long code2 = hash(kmerRev);
				String km;
				if(code2 <= code) {// select the kmer 
					//km = longToBytes(kmer);
					km = Long.toBinaryString(kmer);
				}else { 
					//km = longToBytes(kmerRev);
					km = Long.toBinaryString(kmerRev);

				}
				int len2 = 0; 
				long minCode = Long.MIN_VALUE;
				while(km.length() < 2*k) {
					km = "0" + km;
				}
				
				for(int j = 0; j<km.length(); j+=2) {
					
					long x3 = StringEncode(km.charAt(j), km.charAt(j+1));
					minimizer = ((minimizer << 2)| x3) & Wmask;
					len2++;
					if(len2 >= w){
						minCode = hash(minimizer);
						if(minCode > topMinCode) {
							topMinCode = minCode;
						}
					}
				}
				if(topMinCode != PreviousCode) {
					y.add(topMinCode);
					PreviousCode = topMinCode;
				}
			}
		}
	}
	Long[] arr = new Long[y.size()];	
	arr = y.toArray(arr);
	return arr; 		
	}

	public static void main(String args[]) { 
		
		if(args.length != 3) {
			help();
			return;
		}
		final int k = Integer.parseInt(args[0]); // kmer size
		final int w = Integer.parseInt(args[1]); // window size (if less than or equal to zero run kmer only version
		final String Seq = args[2]; // DNA sequence 
		Long[] val = ReadSeq(Seq, k, w); //Place holder variable of Long[] output function
		return;
	}
}


/*
 * %python
# make sure scala UDF works
spark.udf.registerJavaFunction("ckmerudf", "org.jgi.spark.udfs.ckmer2bit", ArrayType(LongType()))
spark.sql("""SELECT ckmerudf("AGCGGGTGGAGGGTGGGANGGTGGTGTGGGTAAGGTGT",5,false)""").show()
*/
